function [dependpkgs] = rxdepend
% 
%   routine 
%       [RxRoot,pkgpath] = ravenx_codec.depend
% 
%   desc
%       routine to chjeck if the package is on the path, and if not, then
%       add it to the path
% 
%  pdugan june          2022            initial


dependpkgs = {
  'ravenx-core',...
  'utilapps',...
};


end

