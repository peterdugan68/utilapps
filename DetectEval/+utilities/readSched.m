function sched = readSched(C)
%
% Description:
%   Extracts handbrowse schedule as an nx4 double precision matrix from xlsread output 
%
% Inputs:
%   C - raw hand browse schedule from Excel worksheet (cell array of char vectors)
%
% Outputs:
%   sched - start and stop date-time for each handbrowse segment (nx4 numeric matrix)
%         - start and stop date-time (and channel) for each handbrowse segment (nx5 numeric matrix)
%
% History
%   msp2  Aug-15-2017   Initial

    % Initializations
    import utilities.*
    [m, n] = size(C);
    assert(m >= 2 && n >= 4,'Handbrowsed Schedule File is not in standard format')
    
    % Find table headers
    tableHeaders = {'Begin Date', 'Begin Time', 'End Date', 'End Time'};
    i = 1;
    while i < m && ~all(strcmpi(C(i, 1:4), tableHeaders))
        i = i + 1;
    end
    assert(all(strcmpi(C(i, 1:4), tableHeaders)) && ~isequal(m, i), ...
        'Handbrowsed Schedule File is not in standard format')
    
    %---
    % Use this code if xlsread was used with "basic" parameter
    %---
% %     sched = [cell2mat(C(i+1:end, 1)) + cell2mat(C(i+1:end, 2)) , ...
% %              cell2mat(C(i+1:end, 3)) + cell2mat(C(i+1:end, 4))];
         
    %---
    % Use this code if xlsread was used without "basic" parameter
    %---
    
    % Pull out body of table
    C2 = C(i+1:end, :);
    
    % Delete unused rows
    C2(cellfun(@(x) any(isnan(x)),C2(:,1)),:) = [];
    
    try
        
        % Prepare handbrowse schedule when handbrowsed by channel
        if n>=5 && any(strcmpi(C(i,5),{'Channels','Channel','Chans','Chan','Ch'}))
            sched = [datenum(C2(:, 1)) + cell2mat(C2(:, 2)) , ...
                 datenum(C2(:, 3)) + cell2mat(C2(:, 4)) , ...
                 cell2mat(C2(:,5))];
            sched = sortrows(sched,[1,3,2]);
            
        % Prepare handbrowse schedule when handbrowsed on all channels
        else
            sched = [datenum(C2(:, 1)) + cell2mat(C2(:, 2)) , ...
                 datenum(C2(:, 3)) + cell2mat(C2(:, 4))];
            sched = sortrows(sched);
        end
    catch
        error('Dates in handbrowsed schedule file have an unrecognized format.');
    end
    
    % Error if endpoints of handbrowse records are reversed
    assert(all(sched(:,1)<sched(:,2)),...
        'End Date-Time must be after Begin Date-Time for each handbrowse segment')
        
    % Error if overlapping records in handbrowse schedule
    [m, n] = size(sched);
    if isequal(n,4)
        for i = 1:m-1
            isOverlap = sched(i+1,1)<sched(i,2);
            assert(~isOverlap,'Handbrowse segments must not overlap')
        end
    else
        for i = 1:m-1
            isOverlap = sched(i+1,1)<sched(i,2) && sched(i+1,3)==sched(i,3);
            assert(~isOverlap,'Handbrowse segments must not overlap')
        end
    end
