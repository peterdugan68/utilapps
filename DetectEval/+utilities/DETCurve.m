function [TPR,FPR] = DETCurve(G,I)

disp('***** This code may be completely wrong! *****')

    load('G.dat'); 
    load('I.dat'); 

    % Creates a matrix TPR = zeros(1, 1000); 
    FPR = zeros(1, 1000); 
    P = 1200; 
    N = 1200; 
    index = 0; 

    % Assume the threshold as 0.005 
    for threshold = 0:0.005:1 
        TP = 0; 
        FP = 0; 

        %Provides the genuine users count 
        for i = 1:1200 
            if (G(i) >= threshold) 
                TP = TP + 1; 
            end
        end

        % Provides the Imposters count 
        for i1 = 1:1200 
            if(I(i1) >= threshold) 
                FP = FP + 1; 
            end
        end
        index = index + 1;

        % Calculating true positive rate 
        TPR(index) = TP/P; 

        % Calculating false positive rate 
        FPR(index) = FP/N; 
    end

    % Calculating false negative rate(FNR) using TPR+FNR=1 
    FNR = (1-TPR); 
    x = 0:0.1:1; 
    y = x; 
    [x(i),y(i)] = polyxpoly(x,y,FPR,FNR); 
    fprintf('EER(X): %d n', x(i)); 
    fprintf('EER(Y): %d n', y(i)); 
    plot(FPR,FNR,'LineWidth',2, 'color','g'); 
    hold on; 
    plot(x,y,x,1-y, 'color','r'); 
    plot (x(i),y(i),'X','MarkerSize',10, 'LineWidth', 2,'Color','b'); 
    hold off; 
    title('DET CURVE'); 
    xlabel('False Positive Rate (FPR) '); 
    ylabel('False Neagtive Rate (FNR) ');
end