function d = fastDir(p, r)
% 
% List files in current directory and all subdirectories
%
%   Input
%       p - root directory for search
%       r - recursive directory (true/false)
%
%   Output
%       d - list of files

    if ~exist('r', 'var')
        r = false;
    end
    if r
        option = '/b/s/a-d';
    else
        option = '/b/a-d';
    end
    [status, result] = dos(sprintf('dir "%s" %s', p, option));
    if status
        d = {};
        return;
    end
    fn = strsplit(result, sprintf('\n'))';
    fn(strcmp('', fn)) = [];
    if r
        d = fn;
    else
        d = fullfile(p, fn);
    end