function [threshold, TPtruth, FN, TPtest, FP] = ROCcounts(inc, score, TPtruthScore, isTPtest, lenTruth)
%
% Description:
%   Find score at which TP/FN occur in truth log and TP/FP occur in test log
%
% Inputs:
%   inc - threshold increment for tabulation (scalar)
%   score - scores of test event (numeric vector)
%   TPtruthScore - scores of TP in truth log (numeric vector)
%   isTPtest - classification of test events (TP/FN, logical vector)
%   lenTruth - length of truth log log (scalar)
%
% Outputs:
%   threshold - thresholds used for tabulating event classes (numeric vector)
%   TPtruth - number of TP in truth log at each threshold (numeric vector)
%   FN  - number of FN in truth log at each threshold (numeric vector)
%   TPtest - number of TP in test log at each threshold (numeric vector)
%   FP - number of FP in test log at each threshold (numeric vector)
%
% History
%   msp2  Aug-15-2017   Initial

% Initializations
if lenTruth == 0 || isempty(score)
    threshold = 0:inc:1;
    threshold = threshold';
    threshold = threshold(1:end-1);
    numInc = length(threshold);
    TPtruth = zeros(numInc,1);
    FN = zeros(numInc,1);
    TPtest = zeros(numInc,1);
    FP = ones(numInc,1) .* length(isTPtest);
    return;
end

% Cumulative counts
topScore = 10^ceil(log10(max(score)));
threshold = 0:inc:topScore;
threshold = threshold';
testCount = histcounts(score, threshold);
testTotal = cumsum(testCount, 'reverse')';
TPtruthCount = histcounts(TPtruthScore, threshold);
TPtruth = cumsum(TPtruthCount, 'reverse')'; %TPtruth = hit
FN = lenTruth - TPtruth;         %FN = miss = Type II error
TPtestScore = score(isTPtest);
TPtestCount = histcounts(TPtestScore, threshold);
TPtest = cumsum(TPtestCount, 'reverse')'; 
FP = testTotal - TPtest;       %FP = false alarm = Type I error
threshold = threshold(1:end-1);
