function [truthCfilter, testCfilter] = filterLogs(truthC, testC, param)
%
% Description:
%   Filter only those events falling within time bounds of sched
%
% Inputs:
%   truthC - truth log (cell array of char vectors)
%   testC - test log (cell array of char vectors)
%   param - struct with fields:
%       field: name of field by which events will be filtered (string)
%               if "clock time", use "Begin File" and "File Offset (s)"
%       range: filter events if values are within range (nx2 numeric array)
%       chan: filter events if channel matches
%       logType: 'truth', 'test', or 'both' (string)
%       dataType: 'string', 'double', 'date-time' (string)
%       warn: true/false
%
% Outputs:
%   truthCfilter - filtered truth log (cell array of char vectors)
%   testCfilter - filtered test log (cell array of char vectors)
%
% History
%   msp2  Aug-15-2017   Initial

%---
% Filter events
%---
switch param.logType
    case 'truth'
        truthCfilter = filterEvents(truthC, param);
        testCfilter = testC;
    case 'test'
        truthCfilter = truthC;
        testCfilter = filterEvents(testC, param);        
    case 'both'
        truthCfilter = filterEvents(truthC, param);
        testCfilter = filterEvents(testC, param);
end

%---
% Warn user if events removed from truth log
%---
if param.warn
    lenTruth = size(truthC, 1);
    lenTruthFilter = size(truthCfilter, 1);
    if ~isequal(lenTruth, lenTruthFilter)
        fprintf(2, '\n\n%s', '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
        utilities.fail( ...
            sprintf('%.0f events in truth log fall outside handbrowse schedule', ...
            lenTruth - lenTruthFilter), 'WARNING')
        fprintf(2, '\n%s\n\n\n', '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    end
end

%------------------------------------------------------------------------
function filteredC = filterEvents(C, param)
%
% Description:
%   Filter only those events falling within time bounds of sched
%
% Inputs:
%   C - selection table (cell array of char vectors)
%   param - struct with fields:
%       field: name of field by which events will be filtered (string)
%               if "clock time", use "Begin File" and "File Offset (s)"
%       range: filter events if values are within range (nx2 numeric array)
%       logType: 'truth', 'test', or 'both'
%       dataType: 'string', 'double', 'date-time'
%
% Outputs:
%   filteredC - filtered truth log (cell array of char vectors)

%---
% Initializations
%---
import utilities.*

%---
% Handle log with no events
%---
if isequal(size(C, 1), 1)
    filteredC = C;
    return;
end

%---
% Filter events within range
%---
header = C(1, :);
body = C(2:end, :);
filteredBody = cell(size(body));

% Find values to use for filtering events in selection table
chanEnable = false;
switch param.dataType
    case 'date-time'       
        value = calcRealClockTime(C);
        chanEnable = size(param.range,2)==3;
        if chanEnable
            chan = str2double(get_field(C,'Channel'));
        end
    case 'double'
        value = str2double(get_field(C, param.field));
    otherwise
        value = get_field(C, param.field);
end

% Filter events
switch param.dataType
    case 'string'
        idx = strcmp(value, param.range);
        filteredBody = body(idx,:);
        filteredC = [header ; filteredBody];
    otherwise
        start = 1;
        stop = 0;
        for i = 1:size(param.range, 1)
            currRange = param.range(i, 1:2);
            if chanEnable
                currChan = param.range(i,3);
                idx = value>=currRange(1) & value<currRange(2) & chan==currChan;
            else
                idx = value>=currRange(1) & value<currRange(2);
            end
            numEvents = sum(idx);
            if numEvents > 0
                stop = start + numEvents - 1;
                filteredBody(start:stop, :) = body(idx, :);
                start = stop + 1;
            end
        end

        if stop > 0 % there is at least one filtered event
            filteredBody  = filteredBody (1:stop, :);
            filteredC = [header ; filteredBody];
        else % there are no filtered events
            filteredC = header;
        end
        
end
