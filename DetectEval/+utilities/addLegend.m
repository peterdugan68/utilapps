function addLegend(ah, filterParam)
%
% Adds legend to plot axes
%
% Inputs:
%   ah - vector of axes objects
%   filterParam - struct with fields:
%             value: name of field by which events will be filtered (string)
%                    (if "clock time", use "Begin File" and "File Offset (s)"
%             range: filter events if values are within range (nx2 numeric array)
%
% History
%   msp2  Aug-21-2017   Initial
    
% Find sheet name from filterParam
range = filterParam.range;
switch filterParam.dataType
    case 'date-time'
        fmt = 'yyyymmdd HH:MM:SS';
        legendC = strcat(datestr(range(:,1), fmt), {' - '}, datestr(range(:,2), fmt)); 
        legendTitle = 'Handbrowsed Time Segments';
    case 'double'
        legendC = strcat(num2str(range(:,1)), {' - '}, num2str(range(:,2)));    
        legendTitle = filterParam.field;
    case 'string'
        legendC = range;
        legendC = strrep(legendC, '_', '\_'); %Escape underscores to prevent TeX formatting
        legendTitle = filterParam.field;
    otherwise
        fail('filterParam set up incorrectly for addLegend', 'ERROR')
        return;
end

for i = 1:length(ah)
    lh = legend([ah{i}], legendC, 'Location', 'best');
    title(lh, legendTitle);
end