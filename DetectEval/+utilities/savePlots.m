function savePlots(ah, outPath, type)

%---
% Initializations
%---
param = 'compact';


%---
% Set output paths
%---
outPath = fullfile(outPath, 'figs');
if ~isdir(outPath)
    mkdir(outPath)
end


%---
% Write plots to file and close
%---
for i = 1:length(ah)
    fh = ah{i}.Parent;
    outPathFull = fullfile(outPath, [ah{i}.Tag, '_', type]);
    savefig(fh, outPathFull, param);
    saveas(ah{i},outPathFull, 'png')
    close(fh)
end

