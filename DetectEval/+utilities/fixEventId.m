function newC = fixEventId(C)
%
% Renumber selection IDs if there are any duplicate selection IDs
%
% Input
%   C - selection table (cell array of char vectors)
%
% Output
%   newC - selection table (cell array of char vectors)
%
% History
%   msp2  Aug-15-2017   Initial

import utilities.*
newC = C;
ID = str2double(get_field(C, 'Selection'));
len = length(ID);
uniqueID = unique(ID);
if length(uniqueID) < len
    newC(:, 1) = ['Selection' ; strtrim(cellstr(num2str([1:len]')))];
end
    
    