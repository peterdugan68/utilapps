function ah = writePlots(threshold, TPtruth, FN, TPtest, FP, truthDur, durSound, holdState, ah)
%
% Description:
%   Plot
%      TPR x FP/hr
%      Precision-Recall
%      DET curve
%      TP x threshold
%      FN x threshold
%      FP x threshold
%
% Inputs:
%   threshold - thresholds used for tabulating event classes (numeric vector)
%   TPtruth - number of TP in truth log at each threshold (numeric vector)
%   FN  - number of FN in truth log at each threshold (numeric vector)
%   TPtest - number of TP in test log at each threshold (numeric vector)
%   FP - number of FP in test log at each threshold (numeric vector)
%   truthDur - cumulative duration of TP in days (scalar)
%   durSound - duration of truthed recording for each truth log in days (scalar)
%   holdState - add current plot to previous plot on same axes ('off'/'on')
%   ah - vector of axes objects
%
% History
%   msp2  Aug-15-2017   Initial

%---
% Initializations
%---
import utilities.*
if isempty(ah)
    fh{1} = figure;
    ah{1} = axes(fh{1});
    fh{2} = figure;
    ah{2} = axes(fh{2});
    fh{3} = figure;
    ah{3} = axes(fh{3});
    fh{4} = figure;
    ah{4} = axes(fh{4});
    fh{5} = figure;
    ah{5} = axes(fh{5});
end

%---
% Plot TPR x FP/hr
%---
hold(ah{1}, holdState)
lenTruth = TPtruth + FN;
TPR = TPtruth ./ lenTruth;
FPperHr = FP ./ (durSound .* 24 - truthDur ./ 3600); %FP per hour
plot(ah{1}, FPperHr, TPR, 'Marker', '.', 'MarkerSize', 8);
text(ah{1}, FPperHr + 0.005, TPR - 0.005, num2str(threshold,'%.2f'), ...
    'FontSize', 8, 'Color', [1 0 0]);
title(ah{1}, 'Quasi-ROC')
ah{1}.XLabel.String = 'FP/hr';
ah{1}.YLabel.String = 'TPR';
ah{1}.Tag = 'Quasi_ROC';

%---
% Plot Precision-Recall
%---
hold(ah{2}, holdState)
lenTest = TPtest + FP;
PPV = TPtest ./ lenTest;
plot(ah{2}, TPR, PPV, 'Marker', '.', 'MarkerSize', 8);
text(ah{2}, TPR + 0.001, PPV + 0.02, num2str(threshold,'%.2f'), ...
    'FontSize', 8, 'Color', [1 0 0]);
title(ah{2}, 'Precision-Recall')
ah{2}.XLabel.String = 'Recall';
ah{2}.YLabel.String = 'Precision';
ah{2}.Tag = 'Precision_Recall';

%---
% Plot TPR x threshold
%---
hold(ah{3}, holdState)
plot(ah{3}, threshold, TPR);
title(ah{3}, 'TPR')
ah{3}.XLabel.String = 'Threshold';
ah{3}.YLabel.String = 'TPR';
ah{3}.Tag = 'TPR';

%---
% Plot FNR x threshold
%---
hold(ah{4}, holdState)
plot(ah{4}, threshold, 1-TPR);
title(ah{4}, 'FNR')
ah{4}.XLabel.String = 'Threshold';
ah{4}.YLabel.String = 'FNR';
ah{4}.Tag = 'FNR';

%---
% Plot FP/hr x threshold
%---
hold(ah{5}, holdState)
plot(ah{5}, threshold, FPperHr);
title(ah{5}, 'FP/hr')
ah{5}.XLabel.String = 'Threshold';
ah{5}.YLabel.String = 'FP/hr';
ah{5}.Tag = 'FP_hr';

%---
% % % Plot DET curve
%---
% % FRR = 1 - TPR;
% % FAR = 1 - PPV;
% % ph{6} = plot(FRR, FAR);

% % Holger prefers use of Detector Error Tradeoff (DET) graph rather than ROC.  
% % I haven�t had time to research this plot type, but here�s what I think I�ve 
% % figured out so far.  When I work on Perf Tools, I will be sure to add this plot type!
% % The DET graph plots false reject rate (FRR) by false accept rate (FAR), 
% % which are calculated the same as the familiar false negative rate (FNR) and 
% % false positive rate (FPR). 
% % 
% % The DET graph typically uses logit scales rather than linear scales, 
% % which makes the most interest part of the curve occupy more of the plot 
% % than the ROC curve.  The logit function is calculated as:
% % logit(p) = loge(p/(1-p) = -loge(1/p - 1), where p could be FRR or FAR.

