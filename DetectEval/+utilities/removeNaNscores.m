function C = removeNaNscores(C, fn, input)
%
%  Description:
%    Remove selections that have score = NaN
%
% Inputs
%   C - selection table(cell array of char vectors)
%   input - struct with fields
%       scoreFieldName - (string)
%
% Outputs
%   C - selection table(cell array of char vectors)
%
% History
%   msp2  Oct-10-2017   Initial

% Initializations
import utilities.*
scoreFieldName = input.scoreFieldName;

% Get scores
score = str2double(get_field(C, scoreFieldName));

% Delete selections with score = NaN
if any(isnan(score))
    fail(sprintf( ...
        '%.0f selections with missing scores skipped in test log\n"%s"', ...
        sum(isnan(score)), fn))
    idx = [false ; isnan(score)]; % Add row for header
    C(idx,:) = [];
end

