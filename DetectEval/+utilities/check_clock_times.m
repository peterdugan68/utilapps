function status = check_clock_times(C, fn)
%
% Description:
%   Check if Begin File (or Begin Path) and File Offset match 'Begin Date Time'
%
% Inputs
%   C - selection table to be checked (cell array of char vectors)
%   fn - file name of log (string)
%
% Outputs
%   status - false if OK, true if problem
%
% History
%   msp2  Aug-15-2017   Initial

%---
% Initializations
%---
status = false;
import utilities.*
dateFormat = 'yyyy/mm/dd  HH:MM:SS.FFF';
% % fileOffsetFormat = 'SS.FFF';
fileDateTemplate = '_\d\d\d\d\d\d\d\d_\d\d\d\d\d\d.';
fileDateFormat = '_yyyymmdd_HHMMSS';
secPerDay = 86400;
tolerance = 0.001 / 86400; %2 ms tolerance because default File Offset precision in Raven is 1 ms.

%---
% Check clock times
%---
if size(C, 1) > 1
    beginDT = datenum(get_field(C, 'Begin Date Time'), dateFormat);
    beginFile = get_field(C, 'Begin File');
    fileOffset = str2double(get_field(C, 'File Offset (s)')) ./ secPerDay;
    beginFileDT = regexp(beginFile, fileDateTemplate, 'match');
    beginFileDT = cellfun(@(x) x{1}, beginFileDT, 'UniformOutput', false);
    beginFileDT = datenum(beginFileDT, fileDateFormat);
    if ~all(abs(beginDT - (beginFileDT + fileOffset)) < tolerance)
        fail(sprintf('\n\nWARNING: Begin File and File Offset do not agree with Begin Date Time in:\n  %s\n', fn))
        status = true;
    end
end
