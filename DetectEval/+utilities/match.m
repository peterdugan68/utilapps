function [TPtruthScore, isTPtruth, isTPtest, truthDur, score] = match(truthC, testC, input)
%
% Description:
%   Find score at which TP/FN occur in truth log and TP/FP occur in test log
%
% Inputs:
%   truthC - truth log (cell array of char vectors)
%   testC - test log (cell array of char vectors)
%   input - struct with fiels
%       timeOverlap (scalar)
%       freqOverlap (scalar)
%       scoreFieldName (string)
%
% Outputs:
%   TPtruthScore - score of test event matching each TP event in the 
%                  truth log (numeric vector)
%   isTPtruth - indexes of TP events in the truth log (logical vector)
%   isTPtest - indexes of TP events in the test log (logical vector)
%   truthDur - cumulative duration of TP in days (scalar)
%   score - scores of test events (numeric vector)
%
% History
%   msp2  Aug-15-2017   Initial

%---
% Initializations
%---
import utilities.*
timeOverlap = input.timeOverlap;
freqOverlap = input.freqOverlap;
scoreFieldName = input.scoreFieldName;
TPtruthScore = [];
isTPtruth = false(0);
truthDur = 0;

% Handle empty truth log
if size(truthC, 1) < 2
    isTPtest = false(size(truthC, 1), 1);
    score = str2double(get_field(testC, scoreFieldName));
    return;
end

%---
% Copy data of interest from from truth table
%---
k = 86400;
beginTimeTruth = str2double(get_field(truthC, 'Begin Time (s)'));
endTimeTruth = str2double(get_field(truthC, 'End Time (s)'));
deltaTimeTruth = endTimeTruth - beginTimeTruth;
beginDatetimeTruth = k .* calcRealClockTime(truthC);  % k = s/day
centerDatetimeTruth = beginDatetimeTruth + deltaTimeTruth ./ 2;
lowFreqTruth = str2double(get_field(truthC, 'Low Freq (Hz)'));
highFreqTruth = str2double(get_field(truthC, 'High Freq (Hz)'));
deltaFreqTruth = highFreqTruth - lowFreqTruth;
centerFreqTruth = lowFreqTruth + deltaFreqTruth ./ 2;
channelTruth = str2double(get_field(truthC, 'Channel'));

%---
% Copy data of interest from from test table
%---
beginTimeTest = str2double(get_field(testC, 'Begin Time (s)'));
endTimeTest = str2double(get_field(testC, 'End Time (s)'));
deltaTimeTest = (endTimeTest - beginTimeTest);
beginDatetimeTest = k .* calcRealClockTime(testC);  % k = s/day
centerDatetimeTest = beginDatetimeTest + deltaTimeTest / 2;
lowFreqTest = str2double(get_field(testC, 'Low Freq (Hz)'));
highFreqTest = str2double(get_field(testC, 'High Freq (Hz)'));
deltaFreqTest = highFreqTest - lowFreqTest;
centerFreqTest = lowFreqTest + deltaFreqTest ./ 2;
channelTest = str2double(get_field(testC, 'Channel'));
score = str2double(get_field(testC, scoreFieldName));

% for each truth event
lenTruth = length(centerDatetimeTruth);
isTPtruth = false(lenTruth, 1);
maxMatchScore = inf(lenTruth, 1);
isTPtest = false(length(centerDatetimeTest), 1);
for i = 1:lenTruth
    currMatchIDX = channelTruth(i) == channelTest ...
            & (abs(centerDatetimeTruth(i) - centerDatetimeTest) ...
                <= max(deltaTimeTruth(i), deltaTimeTest) ./ 2 ...
                   + ((0.5 - timeOverlap) * min(deltaTimeTruth(i), deltaTimeTest))) ...
            & (abs(centerFreqTruth(i) - centerFreqTest) ...
                <= max(deltaFreqTruth(i), deltaFreqTest) ./ 2 ...
                   + ((0.5 - freqOverlap) * min(deltaFreqTruth(i), deltaFreqTest)));
    currIsTP = any(currMatchIDX);
    if currIsTP
        isTPtruth(i) = true;
        isTPtest = isTPtest | currMatchIDX;
        maxMatchScore(i) = max(score .* currMatchIDX);
    end
end

% Make vectors characterizing matches in truth log
TPtruthScore = maxMatchScore(isTPtruth);

% Find cumulative duration of TP
truthDur = sum(deltaTimeTruth(isTPtruth));
