function varargout = DetectorValidation(varargin)
% DETECTORVALIDATION MATLAB code for DetectorValidation.fig
%      DETECTORVALIDATION, by itself, creates a new DETECTORVALIDATION or raises the existing
%      singleton*.
%
%      H = DETECTORVALIDATION returns the handle to a new DETECTORVALIDATION or the handle to
%      the existing singleton*.
%
%      DETECTORVALIDATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DETECTORVALIDATION.M with the given input arguments.
%
%      DETECTORVALIDATION('Property','Value',...) creates a new DETECTORVALIDATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DetectorValidation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DetectorValidation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DetectorValidation

% Last Modified by GUIDE v2.5 11-Dec-2017 11:45:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DetectorValidation_OpeningFcn, ...
                   'gui_OutputFcn',  @DetectorValidation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --------------------------------------------------------------------
function DetectorValidation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DetectorValidation (see VARARGIN)

    % Suppress warnings when MATLAB creates an Excel worksheet
    warning('off','MATLAB:xlswrite:AddSheet');

    % Choose default command line output for DetectorValidation
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);

% --------------------------------------------------------------------
function varargout = DetectorValidation_OutputFcn(hObject, eventdata, handles)

    %Add application root directory to search paths
    addpath(fileparts(mfilename('fullpath')))

    % Get default command line output from handles structure
    varargout{1} = handles.output;

% --------------------------------------------------------------------
function detectorValidationFig_CloseRequestFcn(hObject, eventdata, handles)

    %Delete GUI
    delete(hObject);
    
    %Remove application root directory from search paths
    rmpath(fileparts(mfilename('fullpath')))

% --------------------------------------------------------------------
function runButton_Callback(hObject, eventdata, handles)
    utilities.DetectEval(handles)

% --------------------------------------------------------------------
function timeOverlap_Callback(hObject, eventdata, handles)
    overlap = str2double(handles.timeOverlap.String);
    if isnan(overlap) || overlap<0
            handles.timeOverlap.String = '0';
    elseif overlap>1
            handles.timeOverlap.String = '1';
    end
    
% --------------------------------------------------------------------
function freqOverlap_Callback(hObject, eventdata, handles)
    overlap = str2double(handles.freqOverlap.String);
    if isnan(overlap) || overlap<0
            handles.freqOverlap.String = '0';
    elseif overlap>1
            handles.freqOverlap.String = '1';
    end
    
% --------------------------------------------------------------------
function increment_Callback(hObject, eventdata, handles)
    increment = str2double(handles.increment.String);
    if isnan(increment) || increment<=0
        handles.increment.String = '0.05';
    end

% --------------------------------------------------------------------
function inpathSchedule_ButtonDownFcn(hObject, eventdata, handles)

    %--
    % Find current handbrowsed schedule file
    %--
    outFileFull = char(handles.inpathSchedule.UserData);
    if ~exist(outFileFull, 'file')
        outFileFull = fullfile(pwd, 'HandbrowseSchedule.xlsx');
    end

    %--
    % Select new handbrowsed schedule file
    %--
    filterSpec =  '*.xlsx';
    title = 'Select Excel worksheet with handbrowse schedule';
    [outFile, outPath] = uigetfile(filterSpec, title, outFileFull);
    if isequal(outFile, 0)
        return;
    end
    handles.inpathSchedule.String = outFile;
    outFileFull = fullfile(outPath, outFile);
    handles.inpathSchedule.UserData = outFileFull;
    handles.inpathScheduleTitle.TooltipString = outFileFull;

% --------------------------------------------------------------------
function inTruth_ButtonDownFcn(hObject, eventdata, handles)

    %--
    % Find current path for truth logs
    %--
    inTruthPath = char(handles.inTruth.UserData);
    if ~isdir(inTruthPath)
        inTruthPath = pwd;
    end

    %--
    % Select new truth logs for input
    %--
    filterSpect = fullfile(inTruthPath, '*.txt');
    [inTruth, inTruthPath] = uigetfile( ...
        filterSpect, ...
        'Select handbrowsed logs', ...
        'Multiselect', 'on');
    if isequal(inTruth, 0)
        return;
    end
    handles.inTruth.UserData = inTruthPath;
    handles.inTruth.String = inTruth;

% --------------------------------------------------------------------
function inTest_ButtonDownFcn(hObject, eventdata, handles)

    %--
    % Find current file list
    %--
    inTestPath = char(handles.inTest.UserData);
    if ~isdir(inTestPath)
        inTestPath = pwd;
    end

    %--
    % Select new input path
    %--
    filterSpect = fullfile(inTestPath, '*.txt');
    [inTest, inTestPath] = uigetfile( ...
        filterSpect, ...
        'Select detection logs', ...
        'Multiselect', 'on');
    if isequal(inTest, 0)
        return;
    end
    handles.inTest.UserData = inTestPath;
    handles.inTest.String = inTest;

% --------------------------------------------------------------------
function outpath_ButtonDownFcn(hObject, eventdata, handles)

    %--
    % Find current path
    %--
    outpath = char(handles.outpath.UserData);
    if ~isdir(outpath)
        outpath = pwd;
    end

    %--
    % Select new input path
    %--
    outpath = uigetdir(outpath, 'Select folder for output');
    if isequal(outpath, 0)
        return;
    end
    handles.outpath.UserData = outpath;
    handles.outpathTitle.TooltipString = outpath;
    [~, outpathDisplay] = fileparts(outpath);
    handles.outpath.String = outpathDisplay;


% --- Executes on button press in cancelButton.
function cancelButton_Callback(hObject, eventdata, handles)
    delete(hObject.Parent)

% --------------------------------------------------------------------
function classCheckbox_Callback(hObject, eventdata, handles)
    
    % Enable class field
    if hObject.Value
        handles.classFieldName.Enable = 'on';
        handles.classFieldTitle.Enable = 'on';
        
    % Disable class field
    else
        handles.classFieldName.Enable = 'off';
        handles.classFieldTitle.Enable = 'off';
    end

% --- Executes on selection change in snrPopup.
function snrPopup_Callback(hObject, eventdata, handles)

    C = cellstr(hObject.String);
    snrMode = C{hObject.Value};
    switch snrMode
        case '(disabled)'
            handles.snrField.Enable = 'off';
            handles.snrFieldTitle.Enable = 'off';
            handles.snr1a.Enable = 'off';
            handles.snr1b.Enable = 'off';
            handles.snr2a.Enable = 'off';
            handles.snr2b.Enable = 'off';
            handles.snr3a.Enable = 'off';
            handles.snr3b.Enable = 'off';
            handles.snr4a.Enable = 'off';
            handles.snr4b.Enable = 'off';
        case 'Bins'
            handles.snrField.Enable = 'on';
            handles.snrFieldTitle.Enable = 'on';
            handles.snr1a.Enable = 'on';
            handles.snr1b.Enable = 'on';
            handles.snr2a.Enable = 'on';
            handles.snr2b.Enable = 'on';
            handles.snr3a.Enable = 'on';
            handles.snr3b.Enable = 'on';
            handles.snr4a.Enable = 'on';
            handles.snr4b.Enable = 'on';
            handles.snr1a.Style = 'text';
            handles.snr1b.Style = 'edit';
            handles.snr2a.Style = 'text';
            handles.snr2b.Style = 'text';
            handles.snr3a.Style = 'text';
            handles.snr3b.Style = 'text';
            handles.snr4a.Style = 'text';
            handles.snr4b.Style = 'text';
            handles.snr1a.String = '0';
            handles.snr1b.String = 'Inf';
            handles.snr2a.String = '';
            handles.snr2b.String = '';
            handles.snr3a.String = '';
            handles.snr3b.String = '';
            handles.snr4a.String = '';
            handles.snr4b.String = '';
        case 'Thresholds'            
            handles.snrField.Enable = 'on';
            handles.snrFieldTitle.Enable = 'on';
            handles.snr1a.Enable = 'on';
            handles.snr1b.Enable = 'on';
            handles.snr2a.Enable = 'on';
            handles.snr2b.Enable = 'on';
            handles.snr3a.Enable = 'on';
            handles.snr3b.Enable = 'on';
            handles.snr4a.Enable = 'on';
            handles.snr4b.Enable = 'on';
            handles.snr1a.Style = 'edit';
            handles.snr1b.Style = 'text';
            handles.snr2a.Style = 'edit';
            handles.snr2b.Style = 'text';
            handles.snr3a.Style = 'edit';
            handles.snr3b.Style = 'text';
            handles.snr4a.Style = 'edit';
            handles.snr4b.Style = 'text';
            handles.snr1a.String = '0';
            handles.snr1b.String = 'Inf';
            handles.snr2a.String = '';
            handles.snr2b.String = 'Inf';
            handles.snr3a.String = '';
            handles.snr3b.String = 'Inf';
            handles.snr4a.String = '';
            handles.snr4b.String = 'Inf';
    end

% --------------------------------------------------------------------
function snra_Callback(hObject, eventdata, handles)
    
    % Read in SNR from GUI
    snr = str2double(hObject.String);
    
    % If bad SNR, set to blank
    if isnan(snr) || isinf(snr) || snr<0
        hObject.String = '';
    end

% --------------------------------------------------------------------
function snrb_Callback(hObject, eventdata, handles)

    % Find tag of touched control and the next highest control
    thisTagB = hObject.Tag;
    thisIDX = str2double(thisTagB(4));
    prevIDX = thisIDX - 1;
    nextIDX = thisIDX + 1;
    prevTagB = thisTagB;
    prevTagB(4) = num2str(prevIDX);
    thisTagA = thisTagB;
    thisTagA(5) = 'a';
    nextTagB = thisTagB;
    nextTagB(4) = num2str(nextIDX);
    nextTagA = nextTagB;
    nextTagA(5) = 'a';
    
    % Read in SNR from GUI
    thisSNRa = str2double(handles.(thisTagA).String);
    thisSNRb = str2double(handles.(thisTagB).String);
    
    % If bad SNR, set to infinity
    if isnan(thisSNRb)
        thisSNRb = inf;
    end
    
    % If entered SNR is lower than previous cutoff, set to Inf
    if thisSNRb <= thisSNRa
        thisSNRb = Inf;
    end
    
    % Set value back to GUI
    hObject.String = num2str(thisSNRb);
    
    % Set end of next SNR interval
    if isinf(thisSNRb)
        handles.(nextTagA).String = '';
        handles.(nextTagB).String = '';
        handles.(nextTagB).Style = 'text';
        if prevIDX
            handles.(prevTagB).Style = 'edit';
        end
    else
        handles.(nextTagA).String = num2str(thisSNRb);
        handles.(nextTagB).String = 'Inf';
        if thisIDX < 3
            handles.(nextTagB).Style = 'edit';
        end
        if prevIDX
            handles.(prevTagB).Style = 'text';
        end
    end
    
% --------------------------------------------------------------------
function formulasHelp_Callback(hObject, eventdata, handles)
    import utilities.*
    helpMenu('Formulas.html')

% --------------------------------------------------------------------
function outputHelp_Callback(hObject, eventdata, handles)
    import utilities.*
    helpMenu('Output.html')

% --------------------------------------------------------------------
function setup_Callback(hObject, eventdata, handles)
    import utilities.*
    helpMenu('Setup.html')


% --------------------------------------------------------------------
function loadPreset_Callback(hObject, eventdata, handles)
    import utilities.*

    if isempty(which('DeLMAGUI'))
        pDeLMA = uigetdir(pwd, 'Select oDeLMA folder');
        if pDeLMA == 0
            return;
        end
        addpath(genpath(pDeLMA))
    end

    %---
    % Load oDelma sound plan
    %---
    [f,p] = uigetfile('*.mat','Open Sound Project File');
    if f == 0
        return;
    end
    s = load(fullfile(p, f));
    snames = fieldnames(s);
    isDeLMA = cellfun(@(x) isa(s.(x), 'DeLMA'), snames);
    if ~any(isDeLMA)
        warndlg('Project file did not contain a DeLMA project');
        return
    end
    firstDeLMA = find(isDeLMA, 1, 'first');
    myDeLMA = s.(snames{firstDeLMA});

    %---
    % Set preset values into UI
    %---
    
    % Truth log
    [inTruthPath, inTruth] = fileparts(myDeLMA.truth_file);
    handles.inTruth.UserData = inTruthPath;
    handles.inTruth.String = inTruth;
    
    % Test logs
    pBase = myDeLMA.output_dir;
    detectName = char(myDeLMA.iNED_OutputDirMenu);
    h = waitbar(0, 'Finding detection logs');
    d = fastDir(fullfile(pBase, detectName), true);
    [inTestPath, inTest] = cellfun(@fileparts, d, 'UniformOutput', false);
    handles.inTest.UserData = inTestPath;
    handles.inTest.String = inTest;
    delete(h)
    
    % Output directory
    outfolder = 'PerfDir';
    handles.outpath.String = outfolder;
    handles.outpath.UserData = fullfile(pBase, outfolder);
