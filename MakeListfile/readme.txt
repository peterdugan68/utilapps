MakeListfile

MakeListfile is designed to create a text file listing the full path of all files with the ".aif" extension in a user-selected directory tree.

System Requirements: Windows

There are three ways of running MakeListfile:

1. Run "MakeListfile.m" in MATLAB.  "MakeListfile.fig" should be in the same directory.

2. Install windows application by running .\MakeListfile\for_redistribution\MakeListfile_v1p1.exe.  A "MakeListfile" shortcut should appear on your Windows Desktop.

3. Run "_make_listfile.bat" in the root of the directory tree that contains aif files.  MATLAB is not required to run.

-Mike
