function install_ravenx

%%%%%%%%%%%%%%%%%%%%%%%%
%%   RavenX setup     %%
%%
%% Initial creation pdugan 2018
%%
% pdugan ravenx 2022
%%%%%%%%%%%%%%%%%%%%%%%%

FavCat = fileparts(mfilename('fullpath'));
install_path = fullfile(FavCat,'install');

[pth, fldrC, ~] = fileparts(FavCat);
[pth, fldrB, ~] = fileparts(pth);
[pth, fldrA, ~] = fileparts(pth);
FavCat = fullfile(fldrA,fldrB, fldrC);

if ~exist(install_path)
   return; 
else
    addpath(install_path,'-end');
end

install_detEval(FavCat);
install_SelectionTableApp(FavCat);
install_MakeCallcount(FavCat);
install_MakeListfile(FavCat);
install_MakeListfile2(FavCat);
install_pamguideplus(FavCat);



