function [ params ] = SetParams(STFT, N, zp)
% SETPARAMS parameter list specific to Acoustat
% 
% Usage: Converts the STFT parameters defined in the raven package into a
% parameter set defined in Acoustat.  
% 
%
%

% History
%
%  PJD  Initial Creation        Sept. 2017


params.sRate = STFT.Fs;
params.frameRate = params.sRate / (STFT.hop * STFT.nfft);
params.binBW = STFT.Fs / STFT.nfft;
params.totalSamples = N;
params.hopSize = STFT.hop;
params.zeroPad = STFT.zpad; 

end

