function y = mainSet(cmd, name, clickTF, selTF, gramletTF, gramlet, params)
%mainSet	the principal set of Osprey measures
%   This implements the large set of measures that have been part of Osprey for
%   a long time.
%
%   See README.txt in this directory for the calling convention and args.

% % global opLastLoc opLastLocError

switch(cmd)
    case 'init'
        y = struct(...
            'longName',{'file_length' 'sample_rate'	'time'		 'frequency'...
            'amplitude'	'sample_number' 'start_time'	 'end_time'...
            'duration'	'low_frequency'	'high_frequency' 'bandwidth'...
            'energy'	'power'		'peak_frequency' 'peak_time'...
            'peak_amplitude' 'number_of_datalog_entries' 'channel_number'...
            'x_location'	'y_location'	'z_location' ...
            'location_error' }, ...
            'screenName',{'file_length' 'sample_rate'	'time'		'frequency' ...
            'amplitude'	'sample_#'	'start_time'	'end_time'  ...
            'duration'	'low_freq'	'high_freq'	'bandwidth' ...
            'energy'	'power'		'peak_freq'	'peak_time' ...
            'peak_amp'	'#_datalogs'			'channel_#' ...
            'x_loc'		'y_loc'		'z_loc'	...
            'loc_error' }, ...
            'unit', {	's'		'Hz'		's'		'Hz' ...
            'dB'		'',		's'		's'  ...
            's'		'Hz'		'Hz'		'Hz' ...
            'dB'		'dB'		'Hz'		's'  ...
            'dB'		''				'' ...
            'm'		'm'		'm' ...
            '' }, ...
            'type', {	'simple'	'simple'	'point' 	'point' ...
            'point' 	'point' 	'selection'	'selection' ...
            'selection'	'selection'	'selection'	'selection' ...
            'gramlet'	'gramlet'	'gramlet'	'gramlet' ...
            'gramlet'	'simple'			'simple' ...
            'selection'	'selection'	'selection' ...
            'selection' }, ...
            'needSel', num2cell([0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 0 0 1 1 1 1]), ...
            'needGram',num2cell([0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 0]), ...
            'fixTime', num2cell([0 0 1 0 0 0 1 1 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0]), ...
            'enabled', num2cell([1 1 0 0 0 0 0 0 1 0 0 1 1 1 1 1 1 0 0 0 0 0 0]), ...
            'sortIndex',num2cell(1 : 23) );
        
        for i = 1:length(y)
            y(i).longName = strrep(y(i).longName,' ','_');
            y(i).screenName =  strrep(y(i).screenName,' ','_');
        end
        
    case 'measure'
        bx = gramletTF;
        lc = clickTF;
        amp = 20 / log(10);
        sel = selTF;
        
        switch(name)
            case 'file_length'
                v = params.totalSamples / params.sRate;
            case 'sample_rate'
                v = params.sRate;
            case 'time'
                v = lc(1);
            case 'frequency'
                v = lc(2);
            case 'amplitude'
                v = amp * opGetSpect(params.channel, lc(1),lc(1),lc(2),lc(2));
                if (isempty(v)), v = 0; end
            case 'sample_number'
                v = round(lc(1) * params.sRate);
            case 'start_time'
                v = sel(1);
            case 'end_time'
                v = sel(3);
            case 'duration'
                v = abs(sel(3)-sel(1));
            case 'low_frequency'
                v = sel(2);
            case 'high_frequency'
                v = sel(4);
            case 'bandwidth'
                v = abs(sel(4) - sel(2));
            case { 'energy' 'power' }
                d = utils.iff(strcmp(name, 'energy'), 1, utils.nCols(gramlet) / params.frameRate);
                v = amp/2 * log(sum(sum(exp(gramlet).^2)) / d * params.hopSize ...
                    / (1 + params.zeroPad));
            case 'peak_frequency'
                [~,v] = max(gramlet(:));
                nr = utils.nRows(gramlet);
                v = (rem(v-1,nr)       + 0.5) / nr * (bx(4)-bx(2)) + bx(2);
            case 'peak_time'
                [~,v] = max(gramlet(:));
                [nr,nc] = size(gramlet);
                v = (floor((v-1) / nr) + 0.5) / nc * (bx(3)-bx(1)) + bx(1);
            case 'peak_amplitude'
                v = amp * max(max(gramlet));
            case 'number_of_datalog_entries'
                v = params.nlogs;
            case 'channel_number'
                v = params.channel;
            case 'x_location'
                if (length(opLastLoc) >= 1), v = opLastLoc(1); else v = 0; end
            case 'y_location'
                if (length(opLastLoc) >= 2), v = opLastLoc(2); else v = 0; end
            case 'z_location'
                if (length(opLastLoc) >= 3), v = opLastLoc(3); else v = 0;  end
            case 'location_error'
                if (~isempty(opLastLocError)), v = opLastLocError; else v = 0; end
            otherwise
                disp(sprintf('%s: method not found', upper(mfilename)));
                
        end	% name switch
        y = v;
end	% main switch
