%--- Read selection table
function [headers,body] = read_selection_table(inPathFull)
% % function [headers,body,status] = read_selection_table(inPathFull)


%
%  Usage:
%       read_selection_table
%
%  Inputs:
%       inPathFull - full path of selection table (char)
%  Outputs:
%       headers - selection table headers (cell array of string vectors)
%
%  Description:
%       Reads specified Raven selection table fileand returns headers and 
%         body as cell array of string vectors.
%
% History
%   msp2  Jul-19-2017   Replace caseread with textscan to improve speed

%initializations
headers = {};
body = {};

% Open selection table text read mode
fid = fopen(inPathFull,'rt');
assert(~isequal(fid,-1),'\nSelection table could not be read:\n  %s\n',inPathFull)

% Parse selection table by cell into cell vector
C = textscan(fid,'%s','Delimiter','\n');

% Close selection table
fclose(fid);

% Reshape cell vector into cell array
C = regexp(C{1},'\t','split');
m = size(C,1);
n = size(C{1,:},2);
C = reshape([C{:,:}],[n,m])';
headers = C(1,:);
if m>1
    body = C(2:end,:);
end