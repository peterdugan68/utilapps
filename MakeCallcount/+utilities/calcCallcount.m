function [M,C,uniqueTag] = calcCallcount(inFilesFull,sched,tagName)
% Make callcount table from a vector of MATLAB date numbers
%   Input
%     inFilesFull: list of full paths to selection tables (nx1 cell array of strings)
%     sched: site, deploy date, retrieve date, begin analysis date, and 
%             end analysis date for each deployment (1x5 cell array)
%     tagName: name of annotation field to report data by
%
%   Output
%     M: callcount matrix <#channels x #days x #hrsPerDay>
%     C: cell vector where each cell is a callcount matrixes for one of 
%        the unique values of tagName<#channels x #days x #hrsPerDay> 
%     uniqueTag: list of unique annotations in "tagName" field of 
%                Raven selection tables (<nx1> cell)

% Initializations
import utilities.*
chanSched = sched{3};
numChan = length(unique(chanSched));
startDate = sched{4};
stopDate = sched{5};
firstDay = min(startDate);
lastDay = max(stopDate);
numDays = lastDay - firstDay + 1;
numHrs = 24;

% Create matrix where browsed and unbrowsed channel-days are set to 0 
%   and NaNM = NaN(numDays,numChan,numHrs);
M = NaN(numDays,numChan,24);
dStart = startDate-firstDay+1;
dStop = stopDate-firstDay+1;
for i = 1:length(chanSched)
    chan = chanSched(i);
    for d = dStart(i):dStop(i)
        for h = 1:24
            M(d,chan,h) = 0;
        end
    end
end

% Read selection tables
ID = [];
fn = [];
chan = [];
day = [];
hour = [];
tag = {};
for i = 1:length(inFilesFull)
    currFn = {};
	C = read_selection_table(inFilesFull{i});
    check_fields(C)
	C = addBeginFile(C);
    currentID = get_field(C,'Selection');
    [~,f] = fileparts(inFilesFull{i});
    currFn(1:size(C,1)-1,1) = {f};
    currentChan = str2double(get_field(C,'Channel'));
    realtime = calcRealClockTime(C);
    currentHr = floor(mod(realtime,1).*numHrs)+1;
    currentDay = floor(realtime);
    ID = [ID ; currentID];
    fn = [fn ; currFn];
    chan = [chan ; currentChan];
    day = [day ; currentDay];
    hour = [hour; currentHr];
    if ~isempty(tagName)
        currentTag = get_field(C,tagName);
        if isempty(currentTag)
            [~,inFile] = fileparts(inFilesFull{i});
            fail(sprintf('No annotations were found in field "%s" in:\n  %s',...
                tagName,inFile));
        end
        tag = [tag; currentTag];
    end
end

%---
% Count number of events per channel-day
%---
if ~isempty(tagName)
    uniqueTag = unique(tag);
    numTags = length(uniqueTag);
    [~,tagIDX] = ismember(tag,uniqueTag);
    C = {M};
    C(1:numTags,1) = C;
else
    uniqueTag = {};
end

% Initializations
d = floor((day-firstDay)) + 1; % day number for selections (first day = 1)
numEvents = length(d);

% For each Raven selection
for i = 1:numEvents
    if d(i)<1 || d(i)>numDays || isnan(M(d(i),chan(i),hour(i)))
        t = sprintf('WARNING:\nSelection %s falls outside analysis date range in:\n%s',...
            ID{i},fn{i});
        fail(t,'Make Callcount');
        continue;
    end
    if chan(i)>numChan
        t = sprintf('WARNING:\nSelection %s falls outside channel range in:\n%s',...
            ID{i},fn{i});
        fail(t,'Make Callcount');
        continue;
    end
    
    % Add on to callcount matrix
    M(d(i),chan(i),hour(i)) = M(d(i),chan(i),hour(i)) + 1;
    
    % Add on to callcount cellvector of matrixes
    if ~isempty(tagName)
        C{tagIDX(i)}(d(i),chan(i),hour(i)) = C{tagIDX(i)}(d(i),chan(i),hour(i)) + 1;
    end
end
