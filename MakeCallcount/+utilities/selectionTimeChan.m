function [t,chan] = selectionTimeChan(fnFull,annotField)
% Make MATLAB date-time and channel vector for selections in selection table fnFull
%   Input
%     fnFull: full path to selection table
%     annotField: annotation field specified by user
%   Output
%     t: selection times calculated from fnFull
%     chan: selection channels copied from fnFull

% Initializations
t = [];
import utilities.*

% Read seletion Table
C = read_selection_table(fnFull);
if size(C,1) < 2
    return;
end

%---
% Filter selection table by tags specified by user
%---
annotField = annotField{1};
filterName = annotField{1};
filterValue = annotField(2);
annot = get_field(C,filterName);
idx = [true;ismember(annot,filterValue)];
C = C(idx,:);
header = C(1,:);

% Extract File Offset and Begin File from selection table
chan = str2double(get_field(C,'Channel'));
fileOffset = str2double(get_field(C,'File Offset (s)'));
if ismember('Begin File',header)
    beginFile = get_field(C,'Begin File');
else
    beginPath = get_field(C,'Begin Path');
    [~,fn,ext] = fileparts(beginPath);
    beginFile = strcat(fn,ext);
end

% Calculate selection times
fileDateTemplate = {'_\d\d\d\d\d\d\d\d_\d\d\d\d\d\d.' ;
                        '_\d\d\d\d\d\d_\d\d\d\d\d\d.' ;
                        '\d\d\d\d\d\d\d\dT\d\d\d\d\d\dp'};
fileDateFormat = {'_yyyymmdd_HHMMSS';
                  '_yymmdd_HHMMSS';
                  'yyyymmddTHHMMSSp'};
secPerDay = 86400;
beginFileDT = {''};
i = 0;
while i<length(fileDateTemplate) && any(cellfun(@isempty, beginFileDT))
    i = i + 1;
    beginFileDT = regexp(beginFile, fileDateTemplate{i}, 'match');
end
if i<=length(fileDateTemplate)
    beginFileDT = cellfun(@(x) x{1}, beginFileDT, 'UniformOutput', false);
    beginFileDT = datenum(beginFileDT, fileDateFormat{i});
    fileOffsetDT = fileOffset ./ secPerDay;
    t = beginFileDT + fileOffsetDT;
    if strcmp(fileDateTemplate{i}(end),'p')
        fracSec = regexp(beginFile, 'p\d\d\d\d\d\dZ', 'match');
        fracSec = cellfun(@(x) x{1}, fracSec, 'UniformOutput', false);
        fracSec = str2double(regexprep(fracSec,{'p', 'Z'},''));
        fracSec = fracSec ./ 10^6 ./ (24*60*60); 
        t = t + fracSec;
    end
end
