function close(Excel,Workbook)
% Save Excel workbook and close Excel
%   Inputs
%       Excel: Excel application object
%       Workbook: Excel workbook object

    Workbook.Save();
    Workbook.Close();
    Excel.Quit();
    delete(Excel);