function fontColor(Excel,sheetIDX,cellIDX,color)
% Applies font color to indexed cells
%
%   Inputs
%       Excel: Excel application object
%       sheetIDX: Excel worksheet index
%       cellIDX: index indicating which cells are to be centered (<mxn>logical)
%       color: cell color (string, eg, "bright blue","black","wine")
%
%   References
%       http://dmcritchie.mvps.org/excel/colors.htm 
%
%   Note: Loop is required because "get(Worksheet,'Range'" is limited to
%   a string argument of 255 characters are less.

    % Find Excel color index
    switch color
        case 'black'
            excelColor = 1;
        case 'white'
            excelColor = 2;
        case 'gray'
            excelColor = 12;
            
        case 'dark red'
            excelColor = 9;
        case 'dark rose'
            excelColor = 22;
        case 'red'
            excelColor = 3;
        case 'rose'
            excelColor = 38;
            
        case 'green'
            excelColor = 4;
            
        case 'blue'
            excelColor = 5;
        case 'light blue'
            excelColor = 20;
        case 'bright blue'
            excelColor = 33;
        case 'cyan'
            excelColor = 8;
        case 'medium blue'
            excelColor = 37;
        case 'dark blue'
            excelColor = 23;
            
        case 'dark purple'
            excelColor = 11;
        case 'wine'
            excelColor = 13;
        case 'purple'
            excelColor = 39;
        case 'magenta'
            excelColor = 7;
        case 'lavender'
            excelColor = 24;
            
        case 'dark green'
            excelColor = 10;
        case 'medium green'
            excelColor = 43;
        case 'dull green'
            excelColor = 14;
        case 'light green'
            excelColor = 35;
            
        case 'tan'
            excelColor = 40;
        case 'orange'
            excelColor = 46;
        case 'yellow'
            excelColor = 6;
        case 'medium yellow'
            excelColor = 36;
    end
  
    Worksheets = get(Excel,'Worksheets');
    Worksheet = Worksheets.Item(sheetIDX);    
    [m,n] = find(cellIDX);
    assert(~isempty(m),'No true cells in cellIDX')
    cellRef = excel.cellRef([m,n]);
    numCells = length(m);
    first = 1;
    last = min(numCells,40);
    while first <= numCells
        cellRefStr = [sprintf('%s,',cellRef{first:last}),cellRef{last}];
        Range = get(Worksheet,'Range',cellRefStr);
        Range.Font.ColorIndex = excelColor;
        first = last + 1;
        last = min(numCells,first + 40);
    end
