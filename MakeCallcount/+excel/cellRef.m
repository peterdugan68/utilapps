function ref = cellRef(idx)
% Returns Excel cell reference (eg, "A1") given row and column index
%
%   Inputs
%       idx: [row index, column index] (<1x2>double)
%   Outputs
%       ref: Excel cell reference (string, eg "A1")

    [m,n] = size(idx);
    if m == 1
        ref = [excel.col(idx(2)), num2str(idx(1))];
    elseif m > 1
        ref = cell(m,1);
        for i = 1:m
            ref(i) = {excel.cellRef(idx(i,:))};
        end
    else
        error('Cell reference empty in "cellRef"')
    end

    