function colWidth(Excel,sheetIDX,width)
% Set width of selected columnes
%
%   Input
%       Excel: Excel application object
%       sheetIDX: index of worksheet to be modified
%       width: vector of column widths (<1xn> double>, NaN to no change width)

    Worksheets = get(Excel,'Worksheets');
    Worksheet = Worksheets.Item(sheetIDX);

    % Adjust column widths and bold font
    for i = 1:length(width)
        Range = get(Worksheet,'Range',excel.cellRef([1,i]));
        Range.ColumnWidth = width(i);
    end