function writeSoundSpeed(infile, outfile, time, zone, soundSpeed, ...
    soundSpeedUncertainty, numSensorsUsed, description,timeCorrection)
% Write sound speed document needed by Raven locator
%   input:
%     infile - paths of input files
%     outfile - path of output file
%     time - MATLAB date numbers
%     zone - time zone
%     soundSpeed - speeds of sound
%     soundSpeedUncertainty - uncertainty of speeds of sound
%     numSensorsUsed - number of sensors in CTD log
%     description - user text for output file header

    %---
    % Initializations
    %---
    f = 'yyyymmdd  HH:MM:SS';
    descriptionC = cellstr(description);
    if ~iscell(infile)
        infile = {infile};
    end
    
    %---
    % Write SoundSpeed document
    %---
    
    % header
    fid = fopen(outfile, 'w');
    fprintf(fid, '# Raven locator sound speed data\r\n');
    fprintf(fid, '\r\n');
    fprintf(fid, '# Created: %s\r\n',datestr(now,'mmm dd, yyyy HH:MM:SS'));
    fprintf(fid, '# Time Correction: %s\r\n',strtrim(timeCorrection));
    fprintf(fid, '# Time Zone: %s\r\n',zone);
    fprintf(fid, '\r\n');
    fprintf(fid, '# %s\r\n', descriptionC{:});
    fprintf(fid, '\r\n');
    fprintf(fid, '# Input Files:\r\n');
    fprintf(fid, '#    %s\r\n', infile{:});    
    
    % body
    for i = 1:length(time)
        fprintf(fid, '\r\n');
        fprintf(fid, '$begin$\r\n');
        fprintf(fid, '%s %s\r\n', datestr(time(i), f), zone);
        if isequal(numSensorsUsed(i), 1)
            fprintf(fid, '# estimate computed from %.0f sensor:\r\n', numSensorsUsed(i));
        else
            fprintf(fid, '# estimate computed from %.0f sensors:\r\n', numSensorsUsed(i));
        end
        fprintf(fid, '%.0f  %.1f\r\n', soundSpeed(i), soundSpeedUncertainty(i));
        fprintf(fid, '$end\r\n');
    end
    fclose(fid);
end
