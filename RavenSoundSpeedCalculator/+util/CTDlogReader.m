function [time, temp, sal, numSensorsUsed] = CTDlogReader(infile)
%Reads output of CTD logger
%   inputs:  infile         - paths of CTD logs
%   outputs: time           - MATLAB date numbers
%            temp           - temperatures
%            sal            - salinities
%            numSensorsUsed - number of sensors active at measurement

    %---
    %Initializations
    %---
    import util.*
    time = {};
    temp = {};
    sal = {};
    numSensorsUsed = [];

    %---
    % Call this function separately for each input file, and combine results
    %---
    if ~iscell(infile)
        infile = {infile};
    end
    numSensors = length(infile);
    timeC = cell(numSensors, 1);
    tempC = cell(numSensors, 1);
    salC = cell(numSensors, 1);
    for i = 1:numSensors
        [timeC{i}, tempC{i}, salC{i}] = CTDlogReadr(infile{i});
    end

    %---
    % Find all datetimes present in CTD logs
    %---
    time = unique(vertcat(timeC{:}));
    
    %---
    % Put temperatures and salinities in matrices corresponding to time
    %---
    numRecords = length(time);
    temp = NaN(numRecords, numSensors);
    sal = NaN(numRecords, numSensors);
    for i = 1:numSensors
        idx = find(ismember(time, timeC{i}(1)));
        currNumRecords = length(timeC{i});
        temp(idx:idx+currNumRecords-1, i) = tempC{i};
        sal(idx:idx+currNumRecords-1, i) = salC{i};
    end
    
    %---
    % Find number of sensors used for each calculation
    %---
    numSensorsUsed = sum(~isnan(temp'),1);
    
    %---
    % Warn user if number of sensors changes over time
    %---
    if any(any(isnan(temp)))
        
        % Find which which CTD logs do not have readings for entire duration
        [~, fn] = cellfun(@fileparts, infile, 'UniformOutput', false);
        badSensors = fn(any(isnan(temp))');
        
        % Print warning to console
        txt1 = 'The number of active sensors in this array changes over time.';
        txt2 = 'This may lead to abrupt changes in estimated sound speed,';
        txt3 = 'which may result in sudden changes in estimated source location.';
        txt4 = 'These files do not have readings for the entire sample period:';
        txt5 = sprintf('  %s\n', badSensors{:});
        txt = sprintf('%s\n%s\n%s\n\n%s\n%s', txt1, txt2, txt3, txt4, txt5);
        fprintf(2, '\n\nWARNING:\n\n%s\n', txt);
        warndlg(txt, 'WARNING');
    end
end

function [time, temp, sal] = CTDlogReadr(infile)

    %---
    % Read CTD logger output
    %---
    import util.*
    time = [];
    temp = [];
    sal = [];
    C = read_selection_table(infile);
    m = size(C,1) ;
    if m < 2
        fail(sprintf('CTD log is empty:\n  %s', infile));
        return;
    end
    time = cell2mat(cellfun(@datenum, get_field(C,'X'), 'Uniformoutput', false));
    temp = str2double(get_field(C,'Temperature(�C)'));
    sal = str2double(get_field(C,'Salinity(psu)'));
    assert(~isempty(time),sprintf(...
        ['****************************************************************\n',...
        'No "X" column with measurement times was found in\n:\n',...
        '  %s\n', ...
        '****************************************************************\n'],...
        infile));
    assert(~isempty(temp),sprintf(...
        ['****************************************************************\n',...
        'No "Temperature(�C)" column with measurement times was found in\n:\n',...
        '  %s\n', ...
        '****************************************************************\n'],...
        infile));
    assert(~isempty(sal),sprintf(...
        ['****************************************************************\n',...
        'No "Salinity(psu)" column with measurement times was found in\n:\n',...
        '  %s\n', ...
        '****************************************************************\n'],...
        infile));

    %---
    % Warn user if any salinities are equal to zero
    %---
    if any(sal==0)
        [~, fn, ext] = fileparts(infile);
        txt1 = 'A salinity of zero is generally indicative of a sensor out of water.';
        txt2 = 'Please remove all lines in CTD log with salinity = 0';
        txt3 = 'to avoid the calculation of spurious speed of sound values.';
        error('\n\nWARNING: %s %s %s\n  %s%s\n', txt1, txt2, txt3, fn, ext)
    end
end
