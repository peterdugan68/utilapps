function time = correctTime(time0, timeCorrection)
% Offsets input time0 by the number of hours in timeCorrection
%   input:  time 0 - time to be offset
%           timeCorrection - offset in hours
%   output: time - time offset by timeCorrection

    offsetHrs = str2double(timeCorrection(3:5));
    offsetDays = offsetHrs / 24;
    time = time0 + offsetDays;
