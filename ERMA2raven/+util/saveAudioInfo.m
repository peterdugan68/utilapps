function saveAudioInfo(info, listfile)
%
% Cache audioinfo for listfile if it has not already been cached.
%   Input
%       info: audioinfo for listfile
%       listfile: Raven listfile

% create metadata folder if it does not already exist
p = fullfile(fileparts(mfilename('fullpath')),'cache');
funp = which('isfolder');
if ~isempty(funp)
    if ~isfolder(p)
        mkdir(p)
        return;
    end
else
    if ~exist(p,'dir')
        mkdir(p)
        return;
    end
end

% retrieve cached audioinfo
infoC = {};
listfileC = {};
if isfile(fullfile(p,'cache.mat'))
    load(fullfile(p,'cache.mat'), 'infoC', 'listfileC');
end

% add audioinfo for current listfile to cache
infoC = [infoC; {info}];
listfileC = [listfileC ; {listfile}];
f = fullfile(p, 'cache.mat');
save(f, 'infoC', 'listfileC')
