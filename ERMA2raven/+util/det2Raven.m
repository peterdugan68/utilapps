function C = det2Raven(infileFull)
% Reorganize ERMA detection log into Raven selection table format

    % Initializations
    delta_t = 0.1; %event duration in seconds, used to set End Time (s)
    lowFreq = 0; %Low Freq for each selection in seletion table
    highFreq = 24000; %High Freq for each selection in selection table

    % Load click detector log
    log = load(infileFull);
    numEvents = size(log.clicks,1);
    
    % Return if no events in log
    if isequal(numEvents,1) && isequal(log.clicks,[10 0 0 0 0 0])
        C = {};
        return;
    end

    % Construct body selection table
    C = cell(numEvents,14);
    id = 1:numEvents;
    C(:,1) = strtrim(cellstr(num2str(id'))); %Raven selection ID
    C(:,2) = {'Spectrogram 1'}; %Raven sound view
    C(:,3) = {'1'}; %Channel
    t = log.clicks(:,1);
    C(:,4) = cellstr(num2str(t)); %Begin Time
    C(:,5) = cellstr(num2str(t + delta_t)); %End Time
    C(:,6) = cellstr(num2str(lowFreq)); %Low Freq
    C(:,7) = cellstr(num2str(highFreq)); %High Freq
    %C(:,8) - leave blank for Begin Path
    [~,f] = fileparts(infileFull);
    idx = strfind(f,'_');
    f(idx(end):end) = '';
    C(:,9) = {f}; %Begin File
    C(:,10) = cellstr(num2str(log.clicks(:,1))); %File Offset
    C(:,11) = cellstr(num2str(log.clicks(:,2))); %Detection Threshold
    C(:,12) = cellstr(num2str(log.clicks(:,3))); %Amplitude
    C(:,13) = cellstr(num2str(log.clicks(:,4))); %Relative Amplitude
    C(:,14) = cellstr(num2str(log.clicks(:,5))); %ICI
    C(:,15) = cellstr(num2str(log.clicks(:,6))); %???