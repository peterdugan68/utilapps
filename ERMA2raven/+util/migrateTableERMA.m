function C2 = migrateTableERMA(C,fnSoundFull,cumDur)
%
% Migrate Raven selection table by updating Begin Time and End Time 
%   to make compatible with sound file stream.
%
% Inputs
%   C: selection table (cell array of strings)
%   fnSoundFull: full path of sound files in listfile (nx1 cell array of strings)
%   cumDur: cumulative duration of sound files in stream (numeric vector)
%
% Output
%   C2: migrated selection table (cell array of strings)

%---
% Initializations
%---
import util.*
headers = C(1,:);
body = C(2:end,:);
m = size(body,1);
if isequal(m,0)
    C2 = C;
    return;
end


% Fix file extension in Begin Path measurement
[pSound,fnSound,ext] = cellfun(@fileparts,fnSoundFull,'UniformOutput',false);
fnSound = strcat(fnSound,ext);
assert(all(strcmp(ext,ext(1))),...
    'Files in named in Raven list file must all have the same file extention.')
ext = ext(1);
[begin_file,begin_file_idx] = get_field(C,'Begin File');
begin_file = strcat(begin_file,ext);

% Find needed fields in Raven selection table
selection = get_field(C,'Selection');
[begin_time,begin_time_idx] = get_field(C,'Begin Time (s)');
begin_time = str2double(begin_time);
[end_time,end_time_idx] = get_field(C,'End Time (s)');
end_time = str2double(end_time);
[file_offset,file_offset_idx] = get_field(C,'File Offset (s)');
file_offset = str2double(file_offset);

%---
% Test that required fields are present
%---
assert(~isempty(selection),'Not a selection table')
assert(~isempty(begin_time),'No "Begin Time" measurement')
assert(~any(isnan(begin_time)),'"Begin Time" field has some invalid values')
assert(~isempty(end_time),'No "End Time" measurement')
assert(~any(isnan(end_time)),'"End Time" field has some invalid values')
assert(~isempty(file_offset),'No "File Offset" measurement')
assert(~any(isnan(file_offset)),'"File Offset" field has some invalid values')

%---
% Set negative file offset to 0 file offset
%---
if any(file_offset < 0)
    file_offset(file_offset < 0) = 0;
    body(:,file_offset_idx) = cellstr(num2str(file_offset,'%.6f'));
end

%---
% Find number of seconds of recording before each file in stream
%---
if length( cumDur ) > 1
    time_before = [0, cumDur(1:end-1)];
else
    time_before = 0;
end

%---
% Calculate new "Begin Time" and "End Time" for each selection
%--
new_begin_time = zeros( m, 1 );
new_end_time = zeros( m, 1 );
begin_path = cell( m, 1 );
j = 0;
skipped_lines = [];
for i = 1:m
   
    % find begin file in sound file stream
    idx = strcmp( fnSound, begin_file{ i } );
    
    % if no matching sound file found, skip selection
    if ~any( idx )
        skipped_lines = [ skipped_lines ; i ];
        fprintf(2,'WARNING:\nEvent deleted because Begin File not found in listfile:\n  %s\n\n',...
            begin_file{ i });
        continue;
    end
    
    % find number of seconds in file stream before begin file
    file_time = time_before( idx );
    
    % calculate new Begin Time
    curr_begin_time = file_time + file_offset( i );
    curr_end_time = curr_begin_time + diff( [ begin_time( i ), end_time( i ) ] );
    
    % put times in vector
    j = j + 1;
    new_begin_time( j ) = curr_begin_time;
    new_end_time( j ) = curr_end_time;
    
    % update path in Begin Path
    begin_path(j) = {fullfile(fileparts(fnSoundFull{idx}),begin_file{i})};
    
end

% trim skipped lines from "Begin Time" and "End Time" vectors
new_begin_time = new_begin_time( 1:j );
new_end_time = new_end_time( 1:j );
begin_path = begin_path( 1:j );

% Add values to Begin Path field in selection table
begin_path_idx = ismember(headers,'Begin Path');
body(:,begin_path_idx) = begin_path;

%---
% If any lines weren't skipped, replace "Begin Time" and "End Time" columns in table
%---
if ~isempty( new_begin_time )
    
    % If any lines were skipped, delete them from output table
    if ~isempty( skipped_lines )
        body( skipped_lines, : ) = [];
    end
    
    % Update "Begin Time" and "End Time"
    new_begin_time_str = num2str( new_begin_time, '%.6f' );
    new_begin_time_cell = strtrim(cellstr( new_begin_time_str ));
    body( :, begin_time_idx ) = new_begin_time_cell;
    new_end_time_str = num2str( new_end_time, '%.6f' );
    new_end_time_cell = strtrim(cellstr( new_end_time_str ));
    body( :, end_time_idx ) = new_end_time_cell;
    body(:,begin_file_idx) = begin_file;
    
    % Sort by Begin Time
    [~, sortIDX] = sort(new_begin_time);
    body = body(sortIDX, :);

%---
% If all lines were skipped, table is empty
%---
else
    body = {};
end

%---
% Prepend headers to body of migrated selection table
%---
C2 = [headers ; body];
