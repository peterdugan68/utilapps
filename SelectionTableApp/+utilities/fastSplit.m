function fastSplit(inFileFull, N)
%
%   Split selection table into multiple selection tables of size N
%
%       Inputs
%           inFileFull: full path of selection table(s) to be split 
%                       (str or cell column vector of strings
%           cell
%           N: maximum number of selections in output selection tables

% Initializations
import utilities.*

% Accommodate multiple input selection tables
if iscell(inFileFull)
    for i = 1:length(inFileFull)
        fastSplit(inFileFull{i},N)
    end
    return;
end

% Make output directory
[inPath, inFile, ~] = fileparts(inFileFull);
% % inFile = inFile(1:end-11); % delete ".selections" in file name
outPath = inPath;

% Read header
fid = fopen(inFileFull, 'rt'); %open input file
header = textscan(fid, '%s', 1, 'Delimiter', '\n');

% Output valid selection tables with maximum of N events
i = 0;
while ~feof(fid)
    i = i + 1;
    
    % Read next N lines of file (CommentStyle skips extra headers)
	body = textscan(fid, '%s', N, 'CommentStyle', 'Selection', 'Delimiter', '\n');
    
    % Add header to text
    C = [header{1} ; body{1}];
    
    % Output sublog
    outFileFull = fullfile(outPath, sprintf('%s_%03.0f.selections.txt', inFile, i));
    fid2 = fopen(outFileFull, 'wt'); %open output file
    [~,n] = size(C);
    f = [repmat('%s\t',1,n-1),'%s\n'];
    fprintf(fid2, f, C{:,:});
    fclose(fid2); %close output file
end
fclose(fid); %close input file
