function testHeaders(p)
%   Stop execution if first line in each text file is not identical.
%     
%     p = full path of text files to be tested (cellstring vector)

    import utilities.*
    if ~iscell(p)
        p = {p};
    end
    if length(p) < 2
        return;
    end
    
    % Test that selection tables exist
    file(1:length(p),1) = {'file'};
    assert(all(cellfun(@exist, p, file)), ...
        'Not all selection tables exist on path specified.')
    
    C = cellfun(@readFirstLine,p,'Unif',false);
    test = cellfun(@(x) isequal(x,C{1}), C);
    assert(all(test),'Merging selection tables is not possible because header line in\n  "%s"\n\nis not the same as in\n  "%s"\n\n', ...
            p{find(~test,1)}, p{1})

function C = readFirstLine(p)

    % Open selection table text read mode
    fid = fopen(p,'rt');
    assert(~isequal(fid,-1), 'Selection table could not be read:\n  %s\n',p);

    % Parse selection table by cell into cell vector
    C = textscan(fid,'%s',1,'Delimiter','\n');
    
    C = C{1}{1};

    % Close selection table
    fclose(fid);
