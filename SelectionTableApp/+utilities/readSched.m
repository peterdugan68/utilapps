function [sched,chan] = readSched(C)
%
% Description:
%   Extracts handbrowse schedule as an nx4 double precision matrix from xlsread output 
%
% Inputs:
%   C - raw hand browse schedule from Excel worksheet (cell array of char vectors)
%
% Outputs:
%   sched - start and stop date-time for each handbrowse segment (nx4 numeric matrix)
%   chan = channel for each handbrowse segment (nx1 numeric vector)
%
% History
%   msp2  Aug-15-2017   Initial

    % Initializations%---
    sched = [];
    chan = [];
    import utilities.*
    [m, n] = size(C);
    assert(m >= 2 && n >= 4,'Handbrowsed Schedule File is not in standard format')
    
    % Find table headers
    tableHeaders = {'Begin Date', 'Begin Time', 'End Date', 'End Time'};
    i = 1;
    while i < m && ~all(strcmpi(C(i, 1:4), tableHeaders))
        i = i + 1;
    end
    assert(all(strcmpi(C(i, 1:4), tableHeaders)) && ~isequal(m, i), ...
        'Handbrowsed Schedule File is not in standard format')
    
    %---
    % Use this code if xlsread was used with "basic" parameter
    %---
% %     sched = [cell2mat(C(i+1:end, 1)) + cell2mat(C(i+1:end, 2)) , ...
% %              cell2mat(C(i+1:end, 3)) + cell2mat(C(i+1:end, 4))];
         
    %---
    % Use this code if xlsread was used without "basic" parameter
    %---
    
    % Pull out body of table
    C2 = C(i+1:end, :);
    
    % Delete unused rows
    C2(cellfun(@(x) any(isnan(x)),C2(:,1)),:) = [];
    
    % Calculate handbrowse schedule
    try
        sched = [datenum(C2(:, 1)) + cell2mat(C2(:, 2)) , ...
                 datenum(C2(:, 3)) + cell2mat(C2(:, 4))];
    catch
        fail('Dates in handbrowsed schedule file have an unrecognized format.');
        return;
    end

    % Read Channels column, if it exists
    if n>=5 && any(strcmpi(C(i,5),{'Channels','Channel','Chans','Chan','Ch'}))
       chan = cell2mat(C2(:,5));
    end