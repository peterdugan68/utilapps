function [C,startID] = renumberSelectionTable(C,startID,stdViews)
%
%   Renumber selection ID's in selection table using consecutive integers starting with "1".
%
%   Input
%       C: selection table (cell array of strings)
%       startID: new ID for first selection in table
%       stdViews - views found in first selection table read
%   Output
%       C: renumbered selection table (cell array of strings)
%       startID: last ID in selection table + 1

    import utilities.*
        
    m = size(C, 1);
    if m > 1
        
        % Sort selection table in time order
        beginTime = get_field(C, 'Begin Time (s)');
        if ~issorted(beginTime)
            if ~isempty(beginTime)
                [~, idx] = sort(str2double(beginTime));
                idx = [1; idx + 1];
                C = C(idx, :);
            end
        end
        
        % Update Selection ID
        id = zeros(m-1,1);
        N = length(stdViews);
        j = 0;
        for i = 1:N:m-1
            id(i:i+N-1) = startID + j;
            j = j + 1;
        end
        selection_idx = strcmp(C(1,:), 'Selection');
        C(2:end, selection_idx) = strtrim(cellstr(num2str(id)));
        startID = startID + j;
    end
    