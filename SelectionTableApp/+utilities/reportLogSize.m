function reportLogSize(numEvents,d,outpath,scoreEnable,dateFilterEnable,...
    filterEnable,migrateEnable)
% Write TSV file reporting number of selections in each Raven selection table


    import utilities.*

    % Make list of output selection table namess
    [~,fn,ext] = cellfun(@fileparts,d,'UniformOutput',false);
    fn = strcat(fn,ext);
    
    % Make body of log size report
    [m,n] = size(numEvents);
    C = cell(m+7,n+1);
    C(1) = {'Number of Events'};
    C(3,1:2) = {'Run Date:',datestr(now,'mm/dd/yyyy HH:MM:SS')};
    C(5,1:n+1) = {'Unfiltered','Score Filtered','Date Filtered','Annotation Filtered','Migration','Selection Table'};
    C(6,1:n+1) = [compose('%d',sum(numEvents,1)), 'SUMMARY OF ALL SELECTION TABLES'];
    C(8:end,1:n+1) = [compose('%d',numEvents), fn];
    
    % Delete columns for filters that are not enabled
    C(:,logical([false,~scoreEnable,~dateFilterEnable,~filterEnable,~migrateEnable,false])) = [];
         
    % Output log size report
    outfile = 'merged.numevents.csv';
    outfileFull = fullfile(outpath,outfile);
    writeCSV(C,outfileFull)
    