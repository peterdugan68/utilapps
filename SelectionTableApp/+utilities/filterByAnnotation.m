function C2 = filterByAnnotation(C,filterName,filterValue)
%
% Filter selection tables in directory by an annotation field
%   Input
%       C: selection table (cell array of strings)
%       filterName: field to filter selections by
%       filterValue: value to filter selections by
%   Output
%       C2: filtered selection table (cell array of strings)

    % Initializations
    import utilities.*
    
    % Test that annotation fields are present
    assert(all(ismember(filterName,C(1,:))),...
        'Annotation field not present in selection table: \n  %s',filterName);
    
    % Split multiple filter values into separate cell
    filterValue = strtrim(strsplit(filterValue,','));
    
    % Pull annotation vector out of selection table
    annot = get_field(C, filterName);
    
    % Filter selection table using annotations
    idx = [true;ismember(annot,filterValue)];
    C2 = C(idx,:);
    