function raven2table(infile, outfile)
% raven2table converts Raven ArrayGeometry.txt to flat table in XLS format.
%   Raven's ArrayGeometry.txt has one sensor locations per line.
%   formatted2flat creats a new file with one line containing locations for
%   all sensors at each time recorded.

    %read in SensorGeometry.txt as cell array
    f = '%s';
    delim = '\n';
    C = FileUtils.txtread(infile, f, delim);
    [m,~] = size(C);

    %add title to output matrix
    M = {C{1}(3:end)};
    
    %add georeference to output matrix
    i = 2;
    while i<m && ~strcmp(C{i},'$header$')
        i = i + 1;
    end
    start = i + 1;
    i = start;
    while i<m && ~strcmp(C{i},'$end$')
        i = i + 1;
    end
    stop = i - 1;
    test = strncmp(C(start:stop), 'Georeference ', 13);
    idx = find(test);
    idx = idx(1)-1;
    M(3,1:3) = strsplit(C{start+idx});
    
    tableStartRow = 7;
    j = tableStartRow; %j is the row index for the output matrix
    while i<m
        
        %determine if this iteration is the first
        firstPass = isequal(j, tableStartRow);
        
        %if first sensor geolocation, extract format
        if firstPass
            temp = strfind(C(stop+2:end), '# Format is:');
            idx = find(~cellfun(@isempty, temp));
            idx = idx(1);
            str = C{stop+1+idx}(14:end);
            format = strsplit(str);
            [mFormat,nFormat] = size(format);
        end
    
        %extract date, time, sensor locations and sensor location uncertainties
        i = stop + 2;
        while i<m && ~strcmp(C{i},'$begin$')
            i = i + 1;
        end
        start = i + 1;
        i = start;
        while i<m && ~strcmp(C{i},'$end$')
            i = i + 1;
        end
        stop = i - 1;

        %remove comments
        C2 = C(start:stop);
        C2(strncmp(C2,'#',1)) = [];

        %concatenate multi-line section into single row
        [m2,~] = size(C2);
        line = strsplit(C2{1});
        for k = 2:m2
            C3 = strsplit(C2{k});
            line = [line, C3];
        end
        [~,n3] = size(line);
        
        %if first sensor geolocation, put column headers in output matrix
        if firstPass
            header = {'Date', 'Time', 'Zone'};
            header(2,:) = {'', '', ''};
            [~,nHeader] = size(header);
            numSensors = k - 1;
            for kk = 1:numSensors
                header(1,1:nHeader+nFormat) = ...
                    [header(1,:), ...
                     sprintf('Sensor-%02.0f', kk), ...
                     {''}, {''}, {''}, {''}, {''}];
                header(2,:) = [header(2,1:nHeader), format];
                [~,nHeader] = size(header);
            end
            M(tableStartRow-2:tableStartRow-1,1:nHeader) = header;
        end
        
        M(j,1:n3) = (line);
        j = j + 1;
    end
    
    %write output table
    xlswrite(outfile, M)
end