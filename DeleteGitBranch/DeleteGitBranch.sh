# DeleteGitBranch.sh
#
#   Git Bash script for deleting one or more directories and associated history
#
#     To Use:
#       1. Change the following parameters in this script for current case:
#         a. "git clone <path to repository origin>"
#         b. "cd <name of folder git clone creates for local clone>"
#         c. "git filter-branch -r <name of directory(s) to be deleted>"
#         d. "git remote add origin <path to repository origin>"
#       2. Run delete_branch.sh as follows:
#         a. Copy "DeleteGitBranch.sh" to directory in which local clone will be created.
#         b. Launch Git Bash
#         c. Type cd "<path in which local clone will be created and this script has been copied to>"
#         d. Type "DeleteGitBranch.sh" on Git Bash command line.
#       3. Once delete_branch.sh runs successfully (example of successful push will be echoed at end of execution), 
#          run "cd <>" and "git push origin --all --force -v" from command line.
#
#     Based on https://confluence.atlassian.com/display/BITBUCKET/Split+a+repository+in+two

# change directory to parent folder for local clone
cd /E/Bitbucket_GIT/RavenX

# clone remote repo
git clone https://msp2@bitbucket.org/CLO-BRP/ravenx.git

# change directory to local clone
cd /E/Bitbucket_GIT/RavenX/ravenx

# track all branches of remote
for branch in `git branch -a | grep remotes | grep -v HEAD | grep -v master`; do
    git branch --track ${branch##*/} $branch
done
git fetch --all
git pull --all

# delete XBAT_extensions and related history while saving tags (could be directory list rather than single directory)
git remote rm origin
git filter-branch --index-filter 'git rm --cached --ignore-unmatch -r XBAT XBAT_extensions Sedna ProjectGUI Documents' --tag-name-filter cat -- --all
git remote add origin https://msp2@bitbucket.org/CLO-BRP/ravenx.git

# push to remote
git push origin --all --dry-run --force -v

echo
echo
echo ----------------------------------------------------------------------------------------
echo "Run 'cd /E/Bitbucket_GIT/RavenX/ravenx' and 'git push origin --all --force -v' if dry run is successful."
echo
echo "   Note: The other steps in the script should not be rerun.  Once is enough."
echo
echo "   WARNING: 'git push --force' cannot be undone."
echo
echo "   WARNING: Recommend this be run on a copy of repository, not original."
echo
echo "Before running 'cd /E/Bitbucket_GIT/RavenX/ravenx' and 'git push origin --all --force -v', check the following:"
echo "   1. Local clone does not have the deleted directories."
echo "   2. Deleted directories do not appear in log."
echo "   3. Repository size is reduced."
echo "   4. Dry run of 'git push origin --all --dry-run --force -v' was successful."
echo "      	Example of a successful run, which has the remote URL correct and a forced update of every branch."
echo
echo "   		Pushing to https://msp2@bitbucket.org/CLO-BRP/ravenx.git"
echo "   		To https://msp2@bitbucket.org/CLO-BRP/ravenx.git"
echo "    		 + b0d0077...a5c1627 CompressionTask -> CompressionTask (forced update)"
echo "    		 + e68cbfe...b3dda3f DEPLOY -> DEPLOY (forced update)"
echo "    		 + 0345524...0c9cf4b JAZ -> JAZ (forced update)"
echo "    		 + e68cbfe...b3dda3f TEST -> TEST (forced update)"
echo "    		 + 2ebabab...8b243a0 Task_NoiseGUI_dwp22 -> Task_NoiseGUI_dwp22 (forced update)"
echo "    		 + eac3c95...3fa1755 Task_PerfTools-msp2 -> Task_PerfTools-msp2 (forced update)"
echo "    		 + 0260e21...93c967e master -> master (forced update)"
echo
echo
