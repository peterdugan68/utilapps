function Launch_Main(mode)
%
%  Routine:
%       Launch_Main
%
%  Inputs:
%       A string to select path options
%
%  Outputs:
%       Sedna GUI, DeLMA GUI, or batch job
%
%  Description:
%       SetPath_src, routine establishes paths for the PRBA libraries.
%
% History
%   PDugan   Jan 2009
%
%   msp2  9 Oct 2010
%     User options for which copy of XBAT to use.
%
%   msp2 12 Nov 2010
%     Moves XBAT paths to end of PATHDEF
%     Option to SVN update ASE Triton and all modules
%     Option to SVN checkout XBAT in c:\XBAT_R5
%     Help menu with About, Create JIRA issue, known bugs, change password
%     Version number displayed in splash screen and in Help > About
%     Transaction log in GUI
%
%   PDugan   Apr 2011
%     Cleaned up merge and added code to save a configuraiton variable
%     which saves the path of the local XBAT copy. This will be helpful to
%     make sure RA machines stay in sync.
%
%   PDugan   Apr 2011
%     Cleaned up merge and added code to save a configuraiton variable
%     which saves the path of the local XBAT copy. This will be helpful to
%     make sure RA machines stay in sync.
%
%   JZollweg Dec 2011
%     Added 'mode' argument to allow different invocations without editing
%     a file.
%
%   JZollweg August 2014
%     Removed reference to XBAT and revised to allow running in batch
%
%   PDugan Sept 2018
%     Added ALL case to launch all the matlab components in raven-x
%
%   PDugan Apr 2019
%     Added code to paint window title-bar for MATLAB MAIN screen
%
%  Examples
%     Launch_Main - starts ASE_Sedna with the default paths
%     Launch_Main('src') - same as above
%     Launch_Main('detect') - adds detector paths and starts DeLMA
%     Launch_Main('*.mat') - runs a NoiseAnalysis or Detection in batch mode
%     Launch_Main(<anything else>) - same as 'src'


if nargin == 0
    mode = 'desktop';
end

%path up ravenx-core in all launch_main routines.
rxhome = fileparts(fileparts(mfilename('fullpath')));
run(fullfile(rxhome,'ravenx-core','ravenx_setpath_src(mode)'));

[rxroot,pkgpath, dependpkgs] = ravenx_pkgs({'utilapps'}, mode);

% change desktop name, to keep things straight
[pth, nme, ~] = fileparts(pwd);
mDesktop = com.mathworks.mde.desk.MLDesktop.getInstance;
mFrame = mDesktop.get('MainFrame');
mFrame.setTitle(fullfile(pth, nme)); % replace 'MyTitle' with your desired title

% launch rx-app or desktop version
switch (mode)
    
    case {'desktop'}
        
        v = ver('matlab');
        switch(v.Release)
            
            case {'(R2017a)','(R2017b)','(R2018a)'}
                disp(sprintf('Currently running %s , you must have 2020b or later',v.Release));
                
            case {'(R2019b)'}
                hdl = warndlg(sprintf('May have problems with Raven-X using Matlab %s',v.Release),'Matlab version');
                
            otherwise
                % ravenx core package
        end
        
    case {'quiet'}
        
    otherwise
        % app will launch in quiet mode
        
end

