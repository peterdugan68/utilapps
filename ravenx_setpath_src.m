function ravenx_setpath_src(mode)
%  Routine:
%       SetPath_src
%
%  Inputs:
%       mode
%  Outputs:
%       none, but paths for ASE_Sedna are set up
%  Description:
%       SetPath_src, routine establishes paths for the PRBA libraries.
%
% History
%   PDugan       January 2009       Ver 1.0    January 2009
%   JZollweg     March 2011         Ver 1.1    Make usable on non-PCs
%   PDugan       March 2011         Ver 1.2    Added mode variable for
%                                              developers
%   JZollweg     December 2011      Ver 1.3    Made mode variable an input
%   CPopescu     Feb4 2013                     Reverted back
%   JZollweg     August 2014        Ver 2.0    Add batch mode
%   JZollweg     July 2018          Ver 2.1    Customized for RavenX-NA
%   PDugan       Sept 2018                     Merged NA and AD for SPAWAR
%                                              integration.
%   PDugan       3-5-18                        Simplified path scripts
%   PDugan       10-5-19                       Phone config


if nargin < 1
    mode = 'desktop';
end

if ~any(strcmp(mode, {'desktop'}))
    mode = 'desktop';
end

% path up selected utilities.  Skip acoustst bcs we bulled that in through
% ravenx-features.  Feature names were changed to be legal matlab/os names
% and using Acoustst in utilapps will cause issue.  This needs to be
% resolved in future versions.
home_dir = fileparts(mfilename('fullpath')); path([home_dir], path); % root path
DetectEval = [home_dir filesep 'DetectEval']; path([DetectEval], path);  % intern
ERMA2raven = [home_dir filesep 'ERMA2raven']; path([ERMA2raven], path);  % extern
MakeCallcount = [home_dir filesep 'MakeCallcount']; path([MakeCallcount], path);  % extern
MakeListfile = [home_dir filesep 'MakeListfile']; path([MakeListfile], path);  % extern
SelectionTableApp = [home_dir filesep 'SelectionTableApp']; path([SelectionTableApp], path);  % extern





